KO=artifacts/ko.wav
OK=artifacts/ok.wav

SOKOBAN_CLI_EXEC ?= sokoban-cli
SOKOBAN_SDL_EXEC ?= sokoban-sdl
TEST_EXEC ?= unittest

BUILD_DIR ?= ./build

SOKOBAN_SRC_DIRS ?= ./src/sokoban
SOKOBAN_SRCS := $(shell find $(SOKOBAN_SRC_DIRS) -name *.c)
SOKOBAN_OBJS := $(SOKOBAN_SRCS:%=$(BUILD_DIR)/%.o)
SOKOBAN_DEPS := $(SOKOBAN_OBJS:.o=.d)

CLI_SRC_DIRS ?= ./src/cli
CLI_SRCS := $(shell find $(CLI_SRC_DIRS) -name *.c)
CLI_OBJS := $(CLI_SRCS:%=$(BUILD_DIR)/%.o)
CLI_DEPS := $(CLI_OBJS:.o=.d)

SDL_SRC_DIRS ?= ./src/sdl
SDL_SRCS := $(shell find $(SDL_SRC_DIRS) -name *.c)
SDL_OBJS := $(SDL_SRCS:%=$(BUILD_DIR)/%.o)
SDL_DEPS := $(SDL_OBJS:.o=.d)

SOKOBAN_TEST_DIRS ?= ./test/sokoban
TEST_SRCS := $(shell find $(SOKOBAN_TEST_DIRS) -name *.c)
TEST_OBJS := $(TEST_SRCS:%=$(BUILD_DIR)/%.o)
TEST_DEPS := $(TEST_OBJS:.o=.d)

INC_DIRS := $(shell find $(SOKOBAN_SRC_DIRS) -type d)
INC_FLAGS := $(addprefix -I,$(INC_DIRS))

CPPFLAGS ?= $(INC_FLAGS) -MMD -MP

$(BUILD_DIR)/$(SOKOBAN_CLI_EXEC): $(SOKOBAN_OBJS) $(CLI_OBJS)
	$(CC) $(SOKOBAN_OBJS) $(CLI_OBJS) -o $@ $(LDFLAGS) -lncursesw

$(BUILD_DIR)/$(SOKOBAN_SDL_EXEC): $(SOKOBAN_OBJS) $(SDL_OBJS)
	$(CC) $(SOKOBAN_OBJS) $(SDL_OBJS) -o $@ $(LDFLAGS) -lSDL2 -lSDL2_image

$(BUILD_DIR)/$(TEST_EXEC): $(TEST_OBJS) $(SOKOBAN_OBJS)
	$(CC) $(TEST_OBJS) $(SOKOBAN_OBJS) -o $@ $(LDFLAGS) -lcriterion

# c source
$(BUILD_DIR)/%.c.o: %.c
	$(MKDIR_P) $(dir $@)
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@


.PHONY: clean

all: build

build: $(BUILD_DIR)/$(SOKOBAN_CLI_EXEC) $(BUILD_DIR)/$(SOKOBAN_SDL_EXEC) $(BUILD_DIR)/$(TEST_EXEC)

test: build
	@build/unittest && aplay -q $(OK) || aplay -q $(KO)

clean:
	$(RM) -r $(BUILD_DIR)

-include $(DEPS)
-include $(CLI_DEPS)
-include $(TEST_DEPS)

MKDIR_P ?= mkdir -p
