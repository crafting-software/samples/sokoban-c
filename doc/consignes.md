# Consignes
Les consignes suivante sont à suivre pour l'ensemble des TPs concernant le sokoban. Elles sont à suivre
scrupuleusement et entraineront des retraits de points sur votre note dans le cas contraire.  
Je vous rappelle que ce projet est un TP et non un examen, n'hésitez pas à poser des questions à vos professeurs. Je rappelle également que le cours de Technique de Developpement est un cours basé sur l'environnement Linux, de ce fait, votre code sera testé **exclusivement** dans un environnement Linux. Assurez vous donc qu'il fonctionne sur cet environnement. 
## Rendu
Le rendu se fera sur Moodle dans une archive `.tar.gz` et devra suivre la convention de nommage suivante: `NOM1_Prenom1__NOM2_Prenom2.tar.gz`
Cette archive devra contenir:
- Le code source (fichiers .c et .h)
- Le Makefile
- Les fichiers textes correspondant aux niveaux
- Un fichier README.md contenant vos noms et prénoms. 
- Le fichier Doxyfile pour la génératin de documentation
Dans ce fichier, vous pouvez également mettre les diffcultés que vous avez rencontré et comment vous les avez surmontés.  
**INDIQUEZ** également la version de gcc/clang que vous avez utilisez.

La présence de tout autres fichiers sera sanctionné.

{% hint style="tip" %}
Pour créer une archive `tar.gz`, vous devez utiliser la commande `tar`
```shell
tar -cf archive_name.tar.gz file1 file2 ....
```
{% endhint %}
## Documentation
- La documentation doit être rédigé en français
- Toutes les fonctions doivent être documenté (cela inclue les fonctions deja fourni)
- Toutes les structures doivent être documenté
- Pour les fonctions, la documentation doit contenir au minimum un résumé de ce que fait la fonction, explication des paramètres et de la valeur de retour
- Elle doit pouvoir être généré en html via une régle `make`

## Code
- Vous pouvez utiliser n'importe quelle standard C (je vous conseille d'utiliser au minimum le C99)
- Il doit compiler avec `-Wall -Wextra` et sans **AUCUN** warnings
- Les noms de variables, fonctions, structures peuvent être en anglais ou en français. Mais soyez cohérent
- Les noms doivent être clairs et adaptés à leurs utilisations
- Il doit être compréhensible
- Il doit réalisé ce qui est demandé

## Makefile
- Doit utiliser la compilation séparée
- La commande `make` doit compiler l'ensemble du projet et généré l'executable
- Doit posséder une règle `clean`, qui supprime tout les fichiers temporaires
- Doit contenir une règle `doc` pour générer la documentation
- Doit contenir une règle `archive` pour créer l'archive à rendre sur moodle.
- Utiliser les variables `CFLAGS` et `LDFLAGS`

## Programme
- Il ne doit pas crasher
- Il ne doit comporter aucune fuite mémoire (du à votre code)
- Il ne doit comporter aucune erreur mémoire
- Il doit effectuer ce qui est demandé

## Git
- Votre code doit être versionné et pousser sur un dépôt distant gitlab
- Le dépôt ne doit comporter aucun fichier temporaire ni binaire (\*.o, dossier de build, etc)
- Vos messages de commit doivent être en rapport avec les modifications apportés
- Vous devez créer des commits suffisamment souvent, ne faites pas 1 commit par TP
- Tout vos commits doivent compiler sans erreur. Si vous avez fait un commit qui ne compile pas,  **PAS DE PANIQUE**
demandez à votre professeur comment on modifie un commit existant (les warnings sont acceptés dans les commit intermédiaire mais pas sur le commit de rendu)

