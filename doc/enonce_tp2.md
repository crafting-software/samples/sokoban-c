# Sokoban: 2ème partie
Ce deuxième TP noté suit les mêmes consignes que le premier, n'hésitez pas à les relire: [consignes](consignes.md#Consignes)
Dans cette 2ème partie, nous allons mettre en place la possibilité de gagner et ajouter un vrai affichage pour le jeu. La dernière étape sera de donner la possibilité à l'utilisateur de choisir son affichage, sur terminal ou avec un vrai affichage.
## Énoncé
Rappel: Pour gagner un niveau, il faut que tout les points cible sois recouvert par un cube
1. La première étape va donc consister à faire bouger les cubes lorsque le personnage les pousse. Pousser un cube se fera simplement en même temps que le déplacement du personnage.
{% hint style="info"%}
Exemple: Le personnage veut se déplacer d'une case vers la droite mais à sa droite se trouve un cube. Dans ce cas, on déplace le cube et le personnage d'une case vers la droite.
```
#  @$    # // avant
#   @$   # //après
```
{% endhint %}
**ATTENTION**: comme pour le personnage, le cube ne doit pas passer à travers les murs, mais doit pouvoir passer sur les points cible représenté par un `.`

2. Vous allez maintenant faire une fonction qui va vérifier que chaque **points cibles** est bien recouvert par un cube. Pour cela, vous allez devoir modifier la structure `Grid` pour lui ajouter un entier comprenant le nombre de **points cibles** à recouvrir et un autre entier qui comptera le nombre d'entier recouvert. Modifier également la fonction de déplacement du personnage pour incrémenter et décrementer votre compteur. Pour l'instant, lorsque tout les **points cibles** ont été recouvert, on se contentera de fermer le programme. (Il suffit de sortir de la boucle de jeu pour ça)  

3. Vous avez maintenant un jeu totalement fonctionnel. Vous pouvez lire un fichier de niveau, et le résoudre. Nous allons maintenant ajouter un peu de couleur dans tout ça, et surtout un affichage comme vous avez l'habitude d'en voir. Nous allons pour ça utiliser la SDL2, vous trouverez plus d'information [ici](sdl2.md). Lisez bien cette documentation, elle vous apprendra les bases de la SDL2 (que nous avons déja vu en cours). Cette documentation est assez imposante, mais ne vous en faites pas il s'agit de beaucoup de description et finalement très peu de choses à faire.

4. Après avoir lu la documentation fourni pour la [sdl2](sdl2.md), vous devriez avoir créer plusieurs nouvelles règles dans votre Makefile. Une pour compiler les fichiers `sdl2.*` et une autre pour compiler la sdl2 à partir du dossier fourni dans le dépôt git. Maintanant, initialiser et quitter la sdl2 dans votre `main` (Ça aussi, c'est expliqué dans la documentation)

5. Créez maintenant une fonction `display_sdl2` qui prend en paramètre la grille à afficher et qui va afficher votre niveau mais avec la SDL2. N'oubliez pas d'appeler cette fonction dans votre boucle de jeu pour admirer le résultat.
(Vous être libre de choisir les couleurs que vous souhaitez)
{% hint style="tip" %}
Vous allez devoir calculer la taille en pixel de chacune de vos cases, pour ça, la structure `SDLContext` vous fourni la hauteur et la largeur de la fenêtre.
On rappel que l'origine de la fenêtre et de chacun de vos rectangles se trouvent en haut à gauche.
{% endhint %}

6. Maintenant que cela est fait, vous allez gérer les évenements claviers. Pour commencer, créez une fonction `event_sdl2` qui renverra une enum `Event` et ne prendra aucun paramètre. L'enum `Event` devra comporter 6 champs (je les nomme en anglais mais utilisez le français si vous le souhaitez): `Quit`, `Left`, `Right`, `Up`, `Down`, `None`. Cela correspond aux différentes actions que l'on peut effectuer dans le jeu. Cette fonction ne devra pas traiter l'évènement mais renvoyer le type d'action que l'on doit effectuer.

7. Faites maintenant la même chose mais dans une fonction `event` pour la gestion de clavier avec `fgetc` et utilisez cette fonction dans votre boucle de jeu.

8. Au lieu d'appeler `event`, appeler `event_sdl2` et vérifier que votre jeu se comporte de la bonne manière.

9. Vous devriez maintenant avoir 2 pairs de fonctions interchangeables, les fonctions `event` et `event_sdl2` et les fonctions `display` et `display_sdl2`. Si ce n'est pas le cas et bien... revoyez vos fonctions pour que ce le soit. Vous allez créer deux pointeurs de fonctions `handle_event` et `handle_display`, donner leur l'adresse de `event_sdl2` et `display_sdl2` et dans votre boucle de jeu, utilisez les pointeurs de fonctions.

10. Dernière étape du TP ! (vous y êtes presque). Maintenant que l'on peut facilement interchangé nos fonctions grâce aux pointeurs de fonctions, nous allons laisser le choix à l'utilisateur de l'interface qu'il souhaite, en console ou en graphique. Cela sera fait avec un argument en ligne de commande.
```
./sokoban --sdl2
```
doit lancer le jeu avec la sdl2 tandis que 
```
./sokoban
# ou
./sokoban --console
```
doit lancer le jeu en mode console.

