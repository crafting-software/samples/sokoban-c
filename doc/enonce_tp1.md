# Sokoban
Ce TP est à rendre pour le dimanche 20 mars à 23h59 au plus tard.
## Contexte
Ce TP sera le premier d'une série de 5, qui va vous amenez à créer un jeu de sokoban.  
Un jeu de sokoban est un jeu d'énigme ou il faut faire en sorte de placer des cubes sur des emplacements dédié.  
Plus d'informations sur cette [page](https://fr.wikipedia.org/wiki/Sokoban)  
Dans cette série de TP, vous allez manipuler les différents outils que l'on a vu en cours.

**IMPORTANT**
Avant de commencer le TP, lisez les [consignes](../sokoban/consignes.md#Consignes), votre note sera basé sur le respect de ses consignes.
## Énoncé
Pour bien commencer, clonez le repo suivante: [code de base sokoban](https://git.unistra.fr/techdevprintemps2022/SokobanTechDev) puis créez un projet 
sur votre compte gitlab de l'unistra et mettez en tant que Reporter `saigle`, `trevisanjost`, `tsley`.
Une fois cela fait, changez le remote du dépôt local, pour y mettre celui du dépôt que vous venez de créer.
{% hint style="tip" %}
Il faut utiliser la commande `git remote`.
{% endhint %}
Vous trouverez dans ce dépôt, la base de votre TP. 

1. Mettez en place l'environnement pour utiliser l'outil Doxygen
2. Créez ensuite un Makefile capable de compiler le code qui vous a été fourni (n'oubliez pas la règle `clean`)
3. Le code fournit vous permet de lire un fichier qui contient la structure des niveaux de votre jeu.
On vous fournit une structure `Grid` que vous allez devoir remplir avec les informations lu dans le fichier.
Pour cela modifier, la fonction de lecture de fichier. Vous avez le droit de changer son prototype.
Cette structure devra comprend(pour l'instant):
	- Un tableau contenant le type CaseType.
	- Le nombre de colonne
	- Le nombre de ligne
{% hint style="info" %}
- `#` représente un mur
- `$` représente un cube
- `@` représente votre personnage
- `.` représente un objectif (l'endroit ou on doit placer les cubes pour gagner)
- ` ` représente du vide dans lequel votre personnage et les cubes pourront progresser
{% endhint %}
4. Créez une fonction `display` qui prend en paramètre un pointeur vers la structure que vous avez créer et qui vous permet
d'afficher le niveau.
{% hint style="danger" %}
Attention à vos erreurs mémoires!!
{% endhint %}
5. Vous allez maintenant faire bouger votre personnage.  
	a. Créez un module `player`, c'est la que vous mettrez le code pour gérer votre personnage.  
	b. Créez une structure `Player` qui contient une position x et une position y  
	c. Ajoutez un champ `Player` dans la structure `Grid` et remplissez le champ dans la fonction de lecture de fichier  
	d. Créez un `enum` `Direction` avec les champs `Top`, `Left`, `Right`, `Bottom`   
	e. Créez enfin la fonction `move_player` qui prendra en paramètre un pointeur vers le type `Grid` et une `Direction`.  
{% hint style="info" %}
Votre personnage ne doit pas passer à travers les murs, ni à travers les cubes.  
Faites également attention à ce qu'il reste toujours dans votre zone de jeu sinon gare aux SegFault
{% endhint %}
	f. Dans le fichier `main.c`, ajoutez le déplacement de votre personnage.
	Vous pouvez utilisez les touches `h,j,k,l` pour respectivement aller vers la gauche, le bas, le haut, la droite

## Avant de rendre le TP
Vérifiez bien que vous avez bien respectez les [consignes](../sokoban/consignes.md#Consignes) !
