# SDL2
La documentation de la SDL2 se trouve [ici](https://wiki.libsdl.org/CategoryAPI) et vous trouverez des tutoriels [ici](https://wiki.libsdl.org/Tutorials). La suite de ce document vous apprendra les bases de ce dont vous allez avoir besoin pour le TP.
- Récuperez les fichiers présents sur le dépôt suivant [code de base sokoban](https://git.unistra.fr/techdevprintemps2022/SokobanTechDev) sur la branche `partie2`. Vous y trouverez 2 fichiers, `sdl2.c` et `sdl2.h`, ainsi qu'un dossier `SDL2`. `SDL2` est le nom de la bibliothèque qui va nous permettre de gérer une fenêtre, les évènements clavier et l'affichage. Sachez cependant que cette bibliothèque peut également gérer le réseau, le son, l'affichage de texte, etc. (Elle est utilisé dans le monde professionel, c'est notamment cette bibliothèque qui est utilisé dans le jeu `Amnesia the Dark Descent`)
- Nous allons tout d'abord vous montrez comment compiler la SDL2. Pour ça, dans un terminal, aller dans le dossier `SDL2`, vous y trouverez un script `configure`, c'est celui la que nous allons devoir lancer en premier. Cette bibliothèque utilise plusieurs systèmes de compilation dont `autoconf` que nous allons utiliser ici . Lancer le script de cette manière: `./configure --prefix=$PWD/../install_dir`. L'option `prefix` permet de définir le répertoire dans lequel on souhaite que la bibliothèque compiler s'installe (Nous allons l'installer dans un dossier `install_dir` à côté de votre code source). Une fois l'éxecution du script fini, vous pouvez lancer la commande `make install -j6`. Cela va compiler et installer la bibliothèque. le `-j6` permet de lancer la compilation sur 6 cores du processeur (Vous pouvez mettre plus en fonction de votre machine). Si tout c'est bien passé vous pouvez aller voir le dossier `install_dir` et vous y trouverez plusieurs dossiers dont `include` et `lib`. `include` contient les headers nécessaire pour compiler et `lib` contient la bibliothèque que vous devrez lier à votre programme.
{% hint style="tip" %}
./configure --prefix=$PWD/../install\_dir
{% endhint %}
- Maintenant que tout ça est fait, créez une régle dans votre Makefile pour faire cette étape automatiquement. (Votre Makefile ne se trouve pas dans le dossier `SDL2` donc chacune de vos commande devra être précéde de `cd SDL2 && ...`. Par exemple `cd SDL2 && ./configure [ ... ]`
- Créer une autre règle pour compiler le fichier `sdl2.c` et ajouté le fichier `sdl2.o` à l'édition des liens de votre programme. Essayer de compiler, vous verrez que ça échoue. Corrigez le problème. 
- Si vous lancez le programme vous verrez une erreur qui ressemble à `fichier libSDL2.so non trouvé`. C'est tout a fait normal, nous avons vu pourquoi en cours.
{% hint style="tip" %}
Pour corriger le problème, plusieurs solutions:
1. Utilisez `LD_LIBRARY_PATH` pour spécifier le dossier ou se trouve `libSDL2.so` au lancement du programme.
2. Ajoutez `-Wl,-rpath=\$ORIGIN` à l'édition des liens, et copier le fichier `libSDL2.so` à côté de l'executable.
3. Ajoutez `-Wl,-rpath=install_dir/lib` à l'édition des liens.
{% endhint %}
## Dessiner
 Maintenant que vous savez comment compiler et lié la SDL2 à votre programme nous allons parler des bases de la SDL2. Le fichier `sdl2.h` possède 2 fonctions `sdl2_init` et `sdl2_quit`. La première fonction doit être appelé au début du programme et la deuxième doit être appelé à la fin du programme. Appelez ces deux fonctions dans votre `main`.
 Pour la suite, vous allez avoir besoin des fonctions suivantes:
{% hint style="info" %}
`SDL_SetRenderDrawColor(SDLRenderer* renderer, Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha)`; Défini la couleur avec laquelle on va peindre.
alpha correspond à la transparence, mettez le toujours à 255. Tous les paramètres sont entre 0 et 255.

`SDL_RenderClear(SDLRenderer* renderer)`; Dessine la fenêtre avec la couleur décris dans SDL\_SetRenderDrawColor

`SDL_RenderFillRect(SDLRenderer* renderer, SDLRect* rect);` Dessine un rectangle plein configuré par la structure SDLRect.

`SDLRect` possède 4 champs, x: position en x, y: position en y, w: largeur du rectangle, h: hauteur du rectangle. La taille est donnée en nombre de pixel.
`SDL_RenderPresent(SDLRenderer* renderer);` Affiche ce qu'on a dessiner.
{% endhint %}
Une utilisation classique de ces fonctions est la suivante
```c
SDL_SetRenderDrawColor(renderer, 126, 126, 126, 255); // couleur grise
SDL_RenderClear(renderer); // On dessine toute la fenêtre en gris
SDL_Rect rect = {	.x = 0,
					.y = 0,
					.w = 50,
					.h = 50
				};
SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255); // on choisit la couleur rouge
SDL_RenderFillRect(renderer, &rect); // On dessine le rectangle
SDL_RenderPresent(renderer); // On affiche tout
```
Avec ce code, on affiche un carré rouge de taille 50 par 50 à la position (0, 0) sur fond gris.
**IMPORTANT**: Le repère utilisé par la SDL2 possède son origine en haut à gauche de la fenêtre. L'axe des x va de 0 à W (W étant le nombre de pixel en largeur de la fenêtre) en allant de a gauche vers la droite. L'axe des y, lui, va de 0 à H (H étant le nombre de pixel en hauteur) en allant de haut en bas.
![Répère SDL2](SDL2_repere.png)
## Gestion des Evenements
La bibliothèque SDL2 nous permet également de gérer les évènements claviers, nous allons donc voir comment. Nous allons utiliser la fonction
```
SDL_WaitEvent(SDL_Event* event);
```
Cette fonction est bloquante, cela veut dire que tant qu'elle n'a pas detecté un évènement, on ne fais pas avancer le programme. Elle a donc le même comportement que la fonction `fgetc` que nous utilisions dans la première partie du TP.(Il en existe une autre non bloquante que nous n'utiliserons pas pour ce TP). Une fois un évènement detecté, la fonction va remplir la structure `SDL_Event` avec l'évènement correspondant, on pourra donc ensuite tester quel évènement a été déclenché. 2 évènements vont nous interesser et correspondent aux valeurs d'enum suivante: `SDL_QUIT` et `SDL_KEYUP`. Le premier correspond à un appui sur la croix en haut à droite de la fenêtre, et l'autre correspond à l'évènement suivant un relachement d'une touche clavier. 
```c
SDL_Event ev;
SDL_WaitEvent(&ev);
switch (ev.type){
	case SDL_QUIT:
		// on a appuyé sur le bouton "quitter" de la fenêtre
		break;
	case SDL_KEYUP:
		// on a relaché une touche du clavier.
		break;
}
```
On peut tester quel touche a été relaché en utilisant le champ `key.keysym.sym` de la structure `SDL_Event`. Les touches qui vont nous interesser sont les flèches du clavier qui correspondent aux valeurs `SDLK_UP`, `SDLK_DOWN`, `SDLK_LEFT`, `SDLK_RIGHT`.


## À vous de jouer
Vous avez maintenant toutes les clés pour suivre le TP2 et mettre votre jeu sur une vrai interface graphique, je vous renvoie à [l'énoncé du TP](enonce_tp2.md).
