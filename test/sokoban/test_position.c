#include <criterion/criterion.h>
#include <criterion/new/assert.h>
#include "position.h"

Test(position, should_get_column_of) {
  cr_expect(eq(column_of(position(3, 4)), 3));
}

Test(position, should_get_row_of) {
  cr_expect(eq(row_of(position(3, 4)), 4));
}

Test(position, should_be_equal) {
  cr_expect(position_equal(position(3, 3), position(3, 3)));
  cr_expect(not(position_equal(position(3, 3), position(4, 3))));
}

Test(position, should_be_inside_boundaries) {
  struct Position topleft = zero();
  struct Position bottomright = position(5, 5);
  for (int x = 0; x < 5; ++x) {
    for (int y = 0; y < 5; ++y) {
      cr_expect(position_is_inside(position(x, y), topleft, bottomright));
    }
  }
}

Test(position, should_be_outside_boundaries) {
  struct Position topleft = position(0, 0);
  struct Position bottomright = position(5, 5);

  cr_expect(not(position_is_inside(position(-2, 0), topleft, bottomright)));
  cr_expect(not(position_is_inside(position(0, -2), topleft, bottomright)));
  cr_expect(not(position_is_inside(position(40, 0), topleft, bottomright)));
  cr_expect(not(position_is_inside(position(0, 40), topleft, bottomright)));
}

Test(position, should_move_in_all_directions) {
  cr_expect(
      position_equal(position_move(position(0, 0), Top), position(0, -1)));
  cr_expect(
      position_equal(position_move(position(0, 0), Bottom), position(0, 1)));
  cr_expect(
      position_equal(position_move(position(0, 0), Left), position(-1, 0)));
  cr_expect(
      position_equal(position_move(position(0, 0), Right), position(1, 0)));
}
