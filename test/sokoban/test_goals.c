#include <criterion/criterion.h>
#include <criterion/new/assert.h>
#include "goals.h"

Test(goals, should_have_total) {
  cr_expect(eq(i64, goals_total(goals_empty()), 0));

  struct Goals* subject = goals_add(goals_empty(), position(2, 3));
  cr_expect(eq(i64, goals_total(subject), 1));
  goals_destroy(subject);
}

Test(goals, should_not_be_goal_when_no_goal_defined) {
  struct Goals* subject = goals_empty();
  for (int x = 0; x < 100; ++x) {
    for (int y = 0; y < 100; ++y) {
      cr_expect(not(goals_is_goal(subject, position(x, y))));
    }
  }
  goals_destroy(subject);
}

Test(goals, should_be_goal_for_defined_goals) {
  struct Goals* subject = goals_add(goals_empty(), position(2, 3));

  cr_expect(goals_is_goal(subject, position(2, 3)));
  cr_expect(not(goals_is_goal(subject, position(3, 3))));

  goals_destroy(subject);
}

Test(goals, should_not_be_reached_by_default) {
  struct Goals* subject = goals_add(goals_empty(), position(2, 3));

  cr_expect(not(goals_is_reached(subject, position(2, 3))));

  goals_destroy(subject);
}

Test(goals, should_be_reached_when_set) {
  struct Goals* subject = goals_add(goals_empty(), position(2, 3));

  goals_set_reached(subject, position(2, 3), true);
  cr_expect(goals_is_reached(subject, position(2, 3)));

  goals_destroy(subject);
}

Test(goals, should_count_reched_goals) {
  struct Goals* subject =
      goals_add(goals_add(goals_empty(), position(2, 3)), position(3, 3));

  cr_expect(eq(i64, goals_reached(subject), 0));
  goals_set_reached(subject, position(2, 3), true);
  cr_expect(eq(i64, goals_reached(subject), 1));
  goals_set_reached(subject, position(2, 3), false);
  cr_expect(eq(i64, goals_reached(subject), 0));

  goals_destroy(subject);
}
