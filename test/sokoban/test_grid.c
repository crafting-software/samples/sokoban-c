#include <criterion/criterion.h>
#include <criterion/new/assert.h>
#include "grid.h"
#include "grid_loader.h"

Test(grid, should_be_created_empty) {
  struct Grid* subject = grid_new(3, 3);
  for (int x = 0; x < 3; ++x) {
    for (int y = 0; y < 3; ++y) {
      cr_expect(grid_is_empty(subject, position(x, y)));
    }
  }
  grid_destroy(subject);
}

Test(grid, should_have_width) {
  struct Grid* subject = grid_new(4, 3);
  cr_expect(eq(i64, grid_width(subject), 4));
  grid_destroy(subject);
}

Test(grid, should_have_height) {
  struct Grid* subject = grid_new(4, 3);
  cr_expect(eq(i64, grid_height(subject), 3));
  grid_destroy(subject);
}

Test(grid, should_check_if_inside) {
  struct Grid* subject = grid_new(3, 3);
  for (int x = 0; x < 3; ++x) {
    for (int y = 0; y < 3; ++y) {
      cr_expect(grid_is_inside(subject, position(x, y)));
    }
  }
  grid_destroy(subject);
}

Test(grid, should_check_if_outside) {
  struct Grid* subject = grid_new(3, 3);

  cr_expect(not(grid_is_inside(subject, position(-2, 0))));
  cr_expect(not(grid_is_inside(subject, position(0, -2))));
  cr_expect(not(grid_is_inside(subject, position(40, 0))));
  cr_expect(not(grid_is_inside(subject, position(0, 40))));

  grid_destroy(subject);
}

Test(grid, should_be_created_with_player_at_0_0) {
  struct Grid* subject = grid_new(3, 3);
  cr_expect(eq(i64, column_of(grid_get_player(subject)), 0));
  cr_expect(eq(i64, row_of(grid_get_player(subject)), 0));
  grid_destroy(subject);
}

Test(grid, should_load_string) {
  struct Grid* subject = grid_from_string(
      "4 4 0\n"
      "####\n"
      "# $#\n"
      "#.@#\n"
      "####\n");
  cr_expect(grid_is_wall(subject, position(0, 0)));
  cr_expect(grid_is_empty(subject, position(1, 1)));
  cr_expect(grid_is_box(subject, position(2, 1)));
  cr_expect(grid_is_goal(subject, position(1, 2)));
  cr_expect(grid_is_player(subject, position(2, 2)));
  cr_expect(eq(i64, column_of(grid_get_player(subject)), 2));
  cr_expect(eq(i64, row_of(grid_get_player(subject)), 2));
  cr_expect(grid_is_empty(subject, position(2, 2)));
  grid_destroy(subject);
}

Test(grid, should_get_cell_outside_grid) {
  struct Grid* subject = grid_from_string(
      "4 4 0\n"
      "####\n"
      "# $#\n"
      "#.@#\n"
      "####\n");
  cr_expect(grid_is_wall(subject, position(-2, 0)));
  cr_expect(grid_is_wall(subject, position(0, -2)));
  cr_expect(grid_is_wall(subject, position(40, 0)));
  cr_expect(grid_is_wall(subject, position(0, 40)));
  cr_expect(not(grid_is_empty(subject, position(-2, 0))));
  cr_expect(not(grid_is_empty(subject, position(0, -2))));
  cr_expect(not(grid_is_empty(subject, position(40, 0))));
  cr_expect(not(grid_is_empty(subject, position(0, 40))));
  grid_destroy(subject);
}

Test(grid, should_set_cell_inside_grid) {
  struct Grid* subject = grid_new(3, 3);
  for (int x = 0; x < 3; ++x) {
    for (int y = 0; y < 3; ++y) {
      grid_set_wall(subject, position(x, y));
      cr_expect(grid_is_wall(subject, position(x, y)));
    }
  }
  grid_destroy(subject);
}

Test(grid, should_set_cell_outside_grid) {
  struct Grid* subject = grid_new(3, 3);
  grid_set_box(subject, position(-1, 0));
  cr_expect(grid_is_wall(subject, position(-1, 0)));
  cr_expect(not(grid_is_box(subject, position(-1, 0))));
  grid_set_box(subject, position(0, -1));
  cr_expect(grid_is_wall(subject, position(0, -1)));
  cr_expect(not(grid_is_box(subject, position(0, -1))));
  grid_set_box(subject, position(10, 0));
  cr_expect(grid_is_wall(subject, position(10, 0)));
  cr_expect(not(grid_is_box(subject, position(10, 0))));
  grid_set_box(subject, position(0, 10));
  cr_expect(grid_is_wall(subject, position(0, 10)));
  cr_expect(not(grid_is_box(subject, position(0, 10))));
  grid_destroy(subject);
}

Test(grid, grid_move_player_outside_grid_Top) {
  struct Grid* subject = grid_new(1, 1);
  grid_move_player(subject, Top);
  cr_expect(eq(i64, column_of(grid_get_player(subject)), 0));
  cr_expect(eq(i64, row_of(grid_get_player(subject)), 0));
  grid_destroy(subject);
}

Test(grid, grid_move_player_outside_grid_Left) {
  struct Grid* subject = grid_new(1, 1);
  grid_move_player(subject, Left);
  cr_expect(eq(i64, column_of(grid_get_player(subject)), 0));
  cr_expect(eq(i64, row_of(grid_get_player(subject)), 0));
  grid_destroy(subject);
}

Test(grid, grid_move_player_outside_grid_Right) {
  struct Grid* subject = grid_new(1, 1);
  grid_move_player(subject, Right);
  cr_expect(eq(i64, column_of(grid_get_player(subject)), 0));
  cr_expect(eq(i64, row_of(grid_get_player(subject)), 0));
  grid_destroy(subject);
}

Test(grid, grid_move_player_outside_grid_Bottom) {
  struct Grid* subject = grid_new(1, 1);
  grid_move_player(subject, Bottom);
  cr_expect(eq(i64, column_of(grid_get_player(subject)), 0));
  cr_expect(eq(i64, row_of(grid_get_player(subject)), 0));
  grid_destroy(subject);
}

Test(grid, grid_move_player_into_wall) {
  struct Grid* subject = grid_from_string(
      "3 3 0\n"
      "###\n"
      "#@#\n"
      "###\n");
  grid_move_player(subject, Top);
  cr_expect(eq(i64, column_of(grid_get_player(subject)), 1));
  cr_expect(eq(i64, row_of(grid_get_player(subject)), 1));
  grid_destroy(subject);
}

Test(grid, grid_move_player_into_space) {
  struct Grid* subject = grid_from_string(
      "4 3 0\n"
      "####\n"
      "#@ #\n"
      "####\n");
  grid_move_player(subject, Right);
  cr_expect(eq(i64, column_of(grid_get_player(subject)), 2));
  cr_expect(eq(i64, row_of(grid_get_player(subject)), 1));
  grid_destroy(subject);
}

Test(grid, grid_move_player_into_non_movable_box) {
  struct Grid* subject = grid_from_string(
      "4 3 0\n"
      "####\n"
      "#@$#\n"
      "####\n");
  grid_move_player(subject, Right);
  cr_expect(eq(i64, column_of(grid_get_player(subject)), 1));
  cr_expect(eq(i64, row_of(grid_get_player(subject)), 1));
  grid_destroy(subject);
}

Test(grid, grid_move_player_into_movable_box) {
  struct Grid* subject = grid_from_string(
      "5 3 0\n"
      "#####\n"
      "#@$ #\n"
      "#####\n");
  grid_move_player(subject, Right);
  cr_expect(grid_is_player(subject, position(2, 1)));
  cr_expect(grid_is_box(subject, position(3, 1)));
  grid_destroy(subject);
}

Test(grid, grid_is_player) {
  struct Grid* subject = grid_from_string(
      "5 3 0\n"
      "#####\n"
      "#@  #\n"
      "#####\n");
  grid_move_player(subject, Top);
  cr_expect(grid_is_player(subject, position(1, 1)));
  cr_expect(not(grid_is_player(subject, position(2, 1))));
  cr_expect(not(grid_is_player(subject, position(3, 1))));
  cr_expect(not(grid_is_player(subject, position(2, 0))));
  grid_destroy(subject);
}

Test(grid, grid_is_finished) {
  struct Grid* subject = grid_new(3, 3);
  grid_set_goal(subject, position(0, 0));
  grid_set_goal(subject, position(1, 0));
  grid_set_goal(subject, position(2, 0));
  cr_expect(not(grid_is_finished(subject)));
  grid_set_box(subject, position(0, 0));
  cr_expect(not(grid_is_finished(subject)));
  grid_set_box(subject, position(1, 0));
  cr_expect(not(grid_is_finished(subject)));
  grid_set_box(subject, position(2, 0));
  cr_expect(grid_is_finished(subject));

  grid_set_empty(subject, position(2, 0));
  cr_expect(not(grid_is_finished(subject)));

  grid_destroy(subject);
}
