#include <criterion/criterion.h>
#include <criterion/new/assert.h>
#include "grid_loader.h"
#include "grid_render.h"

Test(grid_display, should_cell_render) {
  struct Grid* subject = grid_from_string(
      "4 4 0\n"
      "####\n"
      "# $#\n"
      "#.@#\n"
      "####\n");

  cr_expect(eq(chr, cell_render(subject, position(0, 0)), '#'));
  cr_expect(eq(chr, cell_render(subject, position(1, 1)), ' '));
  cr_expect(eq(chr, cell_render(subject, position(2, 1)), '$'));
  cr_expect(eq(chr, cell_render(subject, position(2, 2)), '@'));
  cr_expect(eq(chr, cell_render(subject, position(1, 2)), '.'));
}
