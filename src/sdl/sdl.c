#include "sdl.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "grid.h"

struct SDLContext {
  int width;
  int height;
  SDL_Window* window;
  SDL_Renderer* renderer;
  SDL_Surface* icons_surface;
  SDL_Texture* icons_texture;
};

struct SDLContext* sdl_init() {
  int const width = 1280;
  int const height = 720;
  struct SDLContext* context = malloc(sizeof(struct SDLContext));

  if (SDL_Init(SDL_INIT_VIDEO)) {
    printf("ERROR: cannot initialize SDL.\n");
    exit(-1);
  }

  SDL_Window* window =
      SDL_CreateWindow("Sokoban", SDL_WINDOWPOS_CENTERED,
                       SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_SHOWN);
  if (!window) {
    printf("ERROR: cannot create window.\n");
    exit(-1);
  }
  SDL_Renderer* renderer =
      SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
  context->width = width;
  context->height = height;
  context->window = window;
  context->renderer = renderer;

  context->icons_surface = IMG_Load("artifacts/sokoban.png");
  if (!context->icons_surface) {
    printf("ERROR: cannot load tileset.\n");
    exit(-1);
  }
  context->icons_texture =
      SDL_CreateTextureFromSurface(context->renderer, context->icons_surface);
  if (!context->icons_texture) {
    printf("ERROR: cannot create texture.\n");
    exit(-1);
  }
  return context;
}

void sdl_destroy(struct SDLContext* context) {
  if (context) {
    SDL_DestroyTexture(context->icons_texture);
    SDL_FreeSurface(context->icons_surface);
    SDL_DestroyWindow(context->window);
    SDL_DestroyRenderer(context->renderer);
    free(context);
    SDL_Quit();
  }
}

void blit(struct SDLContext* context, int x, int y, SDL_Rect* to) {
  SDL_Rect item = {x * 64, y * 64, 64, 64};
  SDL_RenderCopy(context->renderer, context->icons_texture, &item, to);
}

void render_cell(struct SDLContext* context,
                 struct Grid* grid,
                 struct Position p) {
  SDL_Rect where = {.x = p.column * 50, .y = p.row * 50, .w = 50, .h = 50};
  if (grid_is_player(grid, p)) {
    blit(context, 11, 6, &where);
    blit(context, 6, 5, &where);
  } else if (grid_is_wall(grid, p)) {
    blit(context, 7, 6, &where);
  } else if (grid_is_box(grid, p)) {
    if (grid_is_goal(grid, p)) {
      blit(context, 4, 0, &where);
    } else {
      blit(context, 1, 0, &where);
    }
  } else if (grid_is_goal(grid, p)) {
    blit(context, 11, 6, &where);
    blit(context, 0, 3, &where);
  } else {
    blit(context, 11, 6, &where);
  }
}

void sdl_render_grid(struct SDLContext* context, struct Grid* grid) {
  SDL_SetRenderDrawColor(context->renderer, 126, 126, 126, 255);
  SDL_RenderClear(context->renderer);
  for (int row = 0; row < grid_height(grid); ++row) {
    for (int column = 0; column < grid_width(grid); ++column) {
      render_cell(context, grid, position(column, row));
    }
  }
  SDL_RenderPresent(context->renderer);
}

enum Commands sdl_wait_input() {
  while (true) {
    SDL_Event ev;
    SDL_WaitEvent(&ev);
    switch (ev.type) {
      case SDL_QUIT:
        return CommandQuit;
      case SDL_KEYUP:
        switch (ev.key.keysym.sym) {
          case SDLK_UP:
            return CommandTop;
          case SDLK_DOWN:
            return CommandBottom;
          case SDLK_LEFT:
            return CommandLeft;
          case SDLK_RIGHT:
            return CommandRight;
        }
    }
  }
}
