#pragma once
#include "commands.h"

struct SDLContext;
struct Grid;

struct SDLContext* sdl_init();
void sdl_destroy(struct SDLContext* context);
void sdl_render_grid(struct SDLContext* context, struct Grid* grid);
enum Commands sdl_wait_input();
