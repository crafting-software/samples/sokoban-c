#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "commands.h"
#include "grid.h"
#include "grid_loader.h"
#include "sdl.h"

int main(int argc, char* argv[]) {
  if (argc != 2) {
    fprintf(stderr, "ERROR: missing level file.\n");
    fprintf(stderr, "Usage: sokoban <level>\n");
    exit(-1);
  }

  struct Grid* grid = grid_from_file(argv[1]);
  struct SDLContext* context = sdl_init();

  bool run = true;
  while (run) {
    sdl_render_grid(context, grid);
    if (grid_is_finished(grid)) {
      printf("Well done!\n");
      break;
    }

    switch (sdl_wait_input()) {
      case CommandQuit:
        run = false;
        break;
      case CommandLeft:
        grid_move_player(grid, Left);
        break;
      case CommandBottom:
        grid_move_player(grid, Bottom);
        break;
      case CommandTop:
        grid_move_player(grid, Top);
        break;
      case CommandRight:
        grid_move_player(grid, Right);
        break;
    }
  }
  sdl_destroy(context);
  grid_destroy(grid);
}
