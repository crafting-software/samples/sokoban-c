#include "grid.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "goals.h"

/** The cell type
 */
enum CellType {
  WALL = '#',  ///< marker for a wall
  BOX = '$',   ///< marker for a box
  NONE = ' '   ///< marker for empty space
};

/** The grid of sokoban.
 * This structure contains all the data realted to a level of sokoban.
 */
struct Grid {
  enum CellType** cells;   ///< the array of cells
  int width;               ///< the width of the #Grid
  int height;              ///< the height of the #Grid
  struct Position player;  ///< the #Position of the player
  struct Goals* goals;     ///< the list of goals
};

void grid_set_cell(struct Grid* grid,
                   struct Position position,
                   enum CellType value);

struct Grid* grid_new(int width, int height) {
  struct Grid* result = malloc(sizeof(struct Grid));
  result->width = width;
  result->height = height;
  result->player = zero();
  result->cells = malloc(height * sizeof(enum CellType*));
  for (int i = 0; i < height; ++i) {
    result->cells[i] = malloc(width * sizeof(enum CellType));
    for (int j = 0; j < width; ++j) {
      grid_set_cell(result, position(j, i), NONE);
    }
  }
  result->goals = goals_empty();
  return result;
}

void grid_destroy(struct Grid* grid) {
  if (!grid) {
    return;
  }
  if (grid->goals)
    goals_destroy(grid->goals);
  for (int i = 0; i < grid->height; ++i) {
    free(grid->cells[i]);
  }
  free(grid->cells);
  free(grid);
}

int grid_width(struct Grid* grid) {
  return grid->width;
}

int grid_height(struct Grid* grid) {
  return grid->height;
}

bool grid_is_inside(struct Grid* grid, struct Position p) {
  return position_is_inside(p, zero(),
                            position(grid_width(grid), grid_height(grid)));
}

enum CellType grid_get_cell(struct Grid* grid, struct Position position) {
  if (grid_is_inside(grid, position)) {
    return grid->cells[row_of(position)][column_of(position)];
  }
  return WALL;
}

void grid_set_cell(struct Grid* grid,
                   struct Position position,
                   enum CellType value) {
  if (grid_is_inside(grid, position)) {
    grid->cells[row_of(position)][column_of(position)] = value;
  }
}

void grid_set_wall(struct Grid* grid, struct Position position) {
  grid_set_cell(grid, position, WALL);
}

bool grid_is_wall(struct Grid* grid, struct Position position) {
  return grid_get_cell(grid, position) == WALL;
}

void grid_set_goal(struct Grid* grid, struct Position position) {
  grid->goals = goals_add(grid->goals, position);
}

bool grid_is_goal(struct Grid* grid, struct Position position) {
  return goals_is_goal(grid->goals, position);
}

void grid_set_box(struct Grid* grid, struct Position position) {
  goals_set_reached(grid->goals, position, true);
  grid_set_cell(grid, position, BOX);
}

bool grid_is_box(struct Grid* grid, struct Position position) {
  return grid_get_cell(grid, position) == BOX;
}

void grid_set_empty(struct Grid* grid, struct Position position) {
  goals_set_reached(grid->goals, position, false);
  grid_set_cell(grid, position, NONE);
}

bool grid_is_empty(struct Grid* grid, struct Position position) {
  return grid_get_cell(grid, position) == NONE;
}

void grid_set_player(struct Grid* grid, struct Position position) {
  if (grid_is_inside(grid, position)) {
    grid->player = position;
  }
}

struct Position grid_get_player(struct Grid* grid) {
  return grid->player;
}

int grid_is_player(struct Grid* grid, struct Position position) {
  return position_equal(position, grid_get_player(grid));
}

void grid_move_player(struct Grid* grid, enum Direction direction) {
  struct Position new = position_move(grid_get_player(grid), direction);
  if (grid_is_box(grid, new)) {
    grid_push_box(grid, new, direction);
  }
  if (grid_is_empty(grid, new)) {
    grid_set_player(grid, new);
  }
}

void grid_push_box(struct Grid* grid,
                   struct Position position,
                   enum Direction direction) {
  struct Position behind = position_move(position, direction);
  if (grid_is_empty(grid, behind)) {
    grid_set_empty(grid, position);
    grid_set_box(grid, behind);
  }
}

bool grid_is_finished(struct Grid* grid) {
  return goals_reached(grid->goals) == goals_total(grid->goals);
}
