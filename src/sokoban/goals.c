#include "goals.h"
#include <stdlib.h>

/** Linked list of goals
 */
struct Goals {
  struct Position position;  ///< the #Position of the goal
  bool reached;              ///< the status of the goal
  struct Goals* next;        ///< the next goal or null
};

struct Goals* goals_empty() {
  return NULL;
}

struct Goals* goals_add(struct Goals* goals, struct Position p) {
  struct Goals* head = malloc(sizeof(struct Goals));
  head->position = p;
  head->reached = false;
  head->next = goals;
  return head;
}

void goals_destroy(struct Goals* goals) {
  while (goals != NULL) {
    struct Goals* next = goals->next;
    free(goals);
    goals = next;
  }
}

int goals_total(struct Goals* goals) {
  int total = 0;
  while (goals != NULL) {
    ++total;
    goals = goals->next;
  }
  return total;
}

bool goals_is_goal(struct Goals* goals, struct Position p) {
  while (goals != NULL) {
    if (position_equal(p, goals->position)) {
      return true;
    }
    goals = goals->next;
  }
  return false;
}

int goals_reached(struct Goals* goals) {
  int reached = 0;
  while (goals != NULL) {
    if (goals->reached) {
      ++reached;
    }
    goals = goals->next;
  }
  return reached;
}

bool goals_is_reached(struct Goals* goals, struct Position p) {
  while (goals != NULL) {
    if (position_equal(p, goals->position)) {
      return goals->reached;
    }
    goals = goals->next;
  }
  return false;
}

void goals_set_reached(struct Goals* goals, struct Position p, bool status) {
  while (goals != NULL) {
    if (position_equal(p, goals->position)) {
      goals->reached = status;
      return;
    }
    goals = goals->next;
  }
}
