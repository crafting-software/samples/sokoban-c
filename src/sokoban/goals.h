#ifndef GOAL_HEADER
#define GOAL_HEADER

#include <stdbool.h>
#include "position.h"

struct Goals;

/** Get en empty goal list.
 */
struct Goals* goals_empty();

/** Add a new #Position in the goal list.
 *
 * @param goals list of goals where to add the new #Position
 * @param p the #Position
 * @return the new list of goals
 */
struct Goals* goals_add(struct Goals* goals, struct Position p);

/** Deallocate a list of goals.
 *
 * @param goals the list to deallocate, can be null
 */
void goals_destroy(struct Goals* goals);

/** Get the total of goals in the list.
 */
int goals_total(struct Goals* goals);

/** Check if a position is in the goals list.
 */
bool goals_is_goal(struct Goals* goals, struct Position p);

/** Get the number of goals reached.
 */
int goals_reached(struct Goals* goals);

/** Check if a position is a reached goal.
 */
bool goals_is_reached(struct Goals* goals, struct Position p);

/** Set the status of a goal at given #Position.
 * If the given #Position is a goal, then set the reached status,
 * do nothing otherwise.
 *
 * @param goals the list of goals
 * @param p the #Position for the new status
 * @param status the new reached status
 */
void goals_set_reached(struct Goals* goals, struct Position p, bool status);

#endif
