#ifndef POSITION_HEADER
#define POSITION_HEADER

#include <stdbool.h>
#include "direction.h"

/** A Position in a #Grid
 */
struct Position {
  int column;  ///< the column component
  int row;     ///< the row component
};

/** Get the (0, 0) position.
 */
struct Position zero();

/** Create a position.
 */
struct Position position(int column, int row);

/** Get the row of a position
 */
int row_of(struct Position position);

/** Get the column of a position
 */
int column_of(struct Position position);

/** Check if two position are equal.
 */
int position_equal(struct Position left, struct Position right);

/** Check if a position is inside a rect.
 * Return trus if topleft <= position < bottomright.
 * @param position the position to check
 * @param topleft the top left coordinate of the rect
 * @param bottomright the bottom right coordinate of the rect
 * @return true if the position is inside the rect
 */
bool position_is_inside(struct Position position,
                        struct Position topleft,
                        struct Position bottomright);

/** Compute a new position after a move in given #Direction.
 */
struct Position position_move(struct Position position,
                              enum Direction direction);

#endif
