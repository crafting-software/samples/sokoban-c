#ifndef GRID_HEADER
#define GRID_HEADER

#include "direction.h"
#include "position.h"

#include <stdbool.h>

struct Grid;

/** Allocate a new grid of the given size.
 * The new grid is empty, the player is at (0, 0) and there is no goals defined.
 *
 * @param width the width of the grid
 * @param height the height of the grid
 */
struct Grid* grid_new(int width, int height);

/** Desallocate a grid.
 *
 * @param grid the grid to desallocate, can be null
 */
void grid_destroy(struct Grid* grid);

/** Get the width of the grid.
 */
int grid_width(struct Grid* grid);

/** Get the height of the grid.
 */
int grid_height(struct Grid* grid);

/** Check if a position is inside a grid.
 */
bool grid_is_inside(struct Grid* grid, struct Position position);

/** Set a wall a the given position in the grid.
 * This function do nothing if the position is outside of the grid.
 *
 * @param grid the grid to modify
 * @param position the coordinate of the new wall
 */
void grid_set_wall(struct Grid* grid, struct Position position);

/** Check if a position is a wall.
 * If the position is outside of the grid, it's always a wall.
 *
 * @param grid the grid
 * @param position the coordinate to check
 */
bool grid_is_wall(struct Grid* grid, struct Position position);

/** Set a goal a the given position in the grid.
 * This function do nothing if the position is outside of the grid.
 *
 * @param grid the grid to modify
 * @param position the coordinate of the new goal
 */
void grid_set_goal(struct Grid* grid, struct Position position);

/** Check if a position is a goal.
 * If the position is outside of the grid, it's never a goal.
 *
 * @param grid the grid
 * @param position the coordinate to check
 */
bool grid_is_goal(struct Grid* grid, struct Position position);

/** Set a box a the given position in the grid.
 * This function do nothing if the position is outside of the grid.
 * If the position is a goal, it will be reached also.
 *
 * @param grid the grid to modify
 * @param position the coordinate of the new box
 */
void grid_set_box(struct Grid* grid, struct Position position);

/** Check if a position is a box.
 * If the position is outside of the grid, it's never a box.
 *
 * @param grid the grid
 * @param position the coordinate to check
 */
bool grid_is_box(struct Grid* grid, struct Position position);

/** Set a empty space a the given position in the grid.
 * This function do nothing if the position is outside of the grid.
 * If the position is a goal, it will be unreached also.
 *
 * @param grid the grid to modify
 * @param position the coordinate of the new empty space
 */
void grid_set_empty(struct Grid* grid, struct Position position);

/** Check if a position is empty.
 * If the position is outside of the grid, it's never empty.
 *
 * @param grid the grid
 * @param position the coordinate to check
 */
bool grid_is_empty(struct Grid* grid, struct Position position);

/** Set the player a the given position in the grid.
 * This function do nothing if :
 * - the position is outside of the grid
 * - the position is a wall
 * - the position is a box
 *
 * @param grid the grid to modify
 * @param position the coordinate of the new empty space
 */
void grid_set_player(struct Grid* grid, struct Position position);

/** Get the player position.
 */
struct Position grid_get_player(struct Grid* grid);

/** Check if a position is a player.
 * If the position is outside of the grid, it's never a player.
 *
 * @param grid the grid
 * @param position the coordinate to check
 */
int grid_is_player(struct Grid* grid, struct Position position);

/** Move the player in the given direction.
 * This function check if the move is legal :
 * - cannont go into wall
 * - can only push one box
 *
 * @param grid the grid
 * @param position the direction to move
 */
void grid_move_player(struct Grid* grid, enum Direction);

/** Push a box in the given direction.
 * The box is pushed only if there is a free space behind.
 * This function updates the goals if needed.
 *
 * @param grid the grid
 * @param position the direction to push
 */
void grid_push_box(struct Grid* grid, struct Position position, enum Direction);

/** Check if all goals are reached.
 */
bool grid_is_finished(struct Grid* grid);

#endif
