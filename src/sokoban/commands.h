#ifndef COMMANDS_HEADER
#define COMMANDS_HEADER

/** List of possible commands
 */
enum Commands {
  CommandQuit = 0,    ///< Quit command
  CommandTop = 1,     ///< Move the player one cell to the top
  CommandLeft = 2,    ///< Move the player one cell left
  CommandRight = 3,   ///< Move the player one cell right
  CommandBottom = 4,  ///< Move the player one cell to the bottom
};

#endif
