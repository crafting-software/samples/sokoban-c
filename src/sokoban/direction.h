#ifndef DIRECTION_HEADER
#define DIRECTION_HEADER

/** List of possible directions.
 */
enum Direction {
  Top = 1,     ///< to the top of a #Position
  Left = 2,    ///< to the left of a #Position
  Right = 3,   ///< to the right of a #Position
  Bottom = 4,  ///< to the bottom of a #Position
};

#endif
