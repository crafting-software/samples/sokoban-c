#ifndef GRID_DISPLAY_HEADER
#define GRID_DISPLAY_HEADER
#include "position.h"

struct Grid;

/** Render a cell a the given #Position.
 */
char cell_render(struct Grid* grid, struct Position position);

/** Render a #Grid to stdout.
 * Used for debug purpose.
 */
void grid_render(struct Grid* grid);

#endif
