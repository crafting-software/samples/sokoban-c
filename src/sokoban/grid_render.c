#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "grid.h"
#include "grid_loader.h"

char cell_render(struct Grid* grid, struct Position p) {
  if (grid_is_player(grid, p)) {
    return '@';
  }

  if (grid_is_wall(grid, p)) {
    return '#';
  }

  if (grid_is_box(grid, p)) {
    if (grid_is_goal(grid, p)) {
      return '*';
    }
    return '$';
  }

  if (grid_is_goal(grid, p)) {
    return '.';
  }

  return ' ';
}

void grid_render(struct Grid* grid) {
  for (int row = 0; row < grid_height(grid); ++row) {
    for (int column = 0; column < grid_width(grid); ++column) {
      printf("%c", cell_render(grid, position(column, row)));
    }
    printf("\n");
  }
}
