#ifndef GRID_LOADER_HEADER
#define GRID_LOADER_HEADER

/** The list of symbols to define a sokoban level.
 */
enum Symbols {
  SYM_WALL = '#',    ///< define a wall
  SYM_BOX = '$',     ///< define a box
  SYM_PLAYER = '@',  ///< define the player position
  SYM_GOAL = '.',    ///< define a goal
  SYM_NONE = ' '     ///< define an empty cell
};

struct Grid;

/** Load a #Grid from string definition.
 */
struct Grid* grid_from_string(char* definition);

/** Load a #Grid from file definition.
 */
struct Grid* grid_from_file(const char* file_path);

#endif
