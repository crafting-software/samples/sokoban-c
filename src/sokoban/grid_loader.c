#include "grid_loader.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "grid.h"

struct Grid* parse_header(char* header) {
  int width = 0;
  int height = 0;
  int number_goals = 0;
  sscanf(header, "%d %d %d", &width, &height, &number_goals);
  return grid_new(width, height);
}

void parse_line(struct Grid* grid, int row, char* line) {
  int column = 0;
  while (*line != 0) {
    switch (*line) {
      case SYM_WALL:
        grid_set_wall(grid, position(column, row));
        break;
      case SYM_BOX:
        grid_set_box(grid, position(column, row));
        break;
      case SYM_PLAYER:
        grid_set_player(grid, position(column, row));
        break;
      case SYM_GOAL:
        grid_set_goal(grid, position(column, row));
        break;
      case SYM_NONE:
        grid_set_empty(grid, position(column, row));
        break;
    }
    ++line;
    ++column;
  }
}

struct Grid* grid_from_string(char* definition) {
  // String copy is necessary because strtok modify the string
  char* buffer = malloc(strlen(definition) + 1);
  strncpy(buffer, definition, strlen(definition) + 1);

  char* line = strtok(buffer, "\n");
  struct Grid* result = parse_header(line);
  int row = 0;
  while ((line = strtok(NULL, "\n")) != NULL) {
    parse_line(result, row, line);
    ++row;
  }
  free(buffer);
  return result;
}

struct Grid* grid_from_file(const char* file_path) {
  FILE* file = fopen(file_path, "r");
  if (!file) {
    fprintf(stderr, "Error %s not found", file_path);
    exit(-1);
  }
  char line[100] = {0};
  fgets(line, 100, file);
  struct Grid* result = parse_header(line);
  int row = 0;
  while (fgets(line, 100, file) != NULL) {
    parse_line(result, row, line);
    row += 1;
  }
  fclose(file);
  return result;
}
