#include "position.h"

struct Position zero() {
  struct Position result = {0, 0};
  return result;
}

struct Position position(int column, int row) {
  struct Position result = {column, row};
  return result;
}

int row_of(struct Position position) {
  return position.row;
}

int column_of(struct Position position) {
  return position.column;
}

int position_equal(struct Position left, struct Position right) {
  return left.column == right.column && left.row == right.row;
}

bool position_is_inside(struct Position position,
                        struct Position topleft,
                        struct Position bottomright) {
  return position.column >= topleft.column && position.row >= topleft.row &&
         position.column < bottomright.column && position.row < bottomright.row;
}

struct Position position_move(struct Position p, enum Direction direction) {
  switch (direction) {
    case Top:
      return position(column_of(p), row_of(p) - 1);
    case Left:
      return position(column_of(p) - 1, row_of(p));
    case Right:
      return position(column_of(p) + 1, row_of(p));
    case Bottom:
      return position(column_of(p), row_of(p) + 1);
  }
}
