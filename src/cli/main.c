#include <locale.h>
#include <ncurses.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include "commands.h"
#include "grid.h"
#include "grid_loader.h"

enum Commands wait_input() {
  while (true) {
    char entry = fgetc(stdin);
    switch (entry) {
      case 'q':
        return CommandQuit;
      case 'h':
        return CommandLeft;
      case 'j':
        return CommandBottom;
      case 'k':
        return CommandTop;
      case 'l':
        return CommandRight;
    }
  }
}

void render_cell(struct Grid* grid, struct Position p) {
  if (grid_is_player(grid, p)) {
    mvaddstr(p.row, p.column, "\xE2\x97\x89");
  } else if (grid_is_wall(grid, p)) {
    mvaddstr(p.row, p.column, "\xE2\x96\x88");
  } else if (grid_is_box(grid, p)) {
    if (grid_is_goal(grid, p)) {
      mvaddstr(p.row, p.column, "\xE2\x96\xA3");
    } else {
      mvaddstr(p.row, p.column, "\xE2\x96\xA0");
    }
  } else if (grid_is_goal(grid, p)) {
    mvaddstr(p.row, p.column, "\xE2\x96\xA2");
  }
}

void grid_display(struct Grid* grid) {
  clear();
  for (int row = 0; row < grid_height(grid); ++row) {
    for (int column = 0; column < grid_width(grid); ++column) {
      render_cell(grid, position(column, row));
    }
  }
  refresh();
}

int main(int argc, char* argv[]) {
  if (argc != 2) {
    fprintf(stderr, "ERROR: missing level file.\n");
    fprintf(stderr, "Usage: sokoban <level>\n");
    exit(-1);
  }

  setlocale(LC_CTYPE, "");
  initscr();
  cbreak();
  noecho();

  struct Grid* grid = grid_from_file(argv[1]);

  bool run = true;
  while (run) {
    grid_display(grid);

    if (grid_is_finished(grid)) {
      wprintf(L"Well done!\n");
      break;
    }

    switch (wait_input()) {
      case CommandQuit:
        run = false;
        break;
      case CommandLeft:
        grid_move_player(grid, Left);
        break;
      case CommandBottom:
        grid_move_player(grid, Bottom);
        break;
      case CommandTop:
        grid_move_player(grid, Top);
        break;
      case CommandRight:
        grid_move_player(grid, Right);
        break;
    }
  }
  grid_destroy(grid);
  endwin();
}
